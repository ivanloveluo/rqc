package com.rq.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.rq.receiver
 * @Description: TODO
 * @date 2019/4/11 10:04
 */
//@Component
@RabbitListener(queues = "hello")
public class HelloReceiver {
    @RabbitHandler
    public void process(String hello) {
        System.out.println("Receiver  : " + hello);
    }
}
