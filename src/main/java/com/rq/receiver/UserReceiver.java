package com.rq.receiver;

import com.model.User;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.rq.receiver
 * @Description: TODO
 * @date 2019/4/11 10:04
 */
@Component
@RabbitListener(queues = "object")
public class UserReceiver {
    @RabbitHandler
    public void process( User user)throws Exception {
        System.out.println(user);
        System.out.println("messages ："+user.toString());
        System.out.println(Thread.currentThread().getName()+"接收到来自topic.message队列的消息: "+user);


    }
}
