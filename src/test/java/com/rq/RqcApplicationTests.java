package com.rq;

import com.rq.receiver.HelloReceiver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RqcApplicationTests {

	@Autowired
	private HelloReceiver helloReceiver;
	@Test
	public void hello() {
	}

}
