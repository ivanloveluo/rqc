package com.rq.receiver;

import com.model.User;
import com.rabbitmq.client.Channel;
import com.rq.utils.JsonUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.rq.receiver
 * @Description: TODO
 * @date 2019/4/11 10:04
 */
@Component
@RabbitListener(queues = "topic.user")
public class UserJsonReceiver {
    @RabbitHandler
    public void process(String jsons, Channel channel, Message message)throws Exception {
        User user = JsonUtils.jsonToPojo(jsons, User.class);
        System.out.println(Thread.currentThread().getName()+"接收到来自topic.message队列的消息: "+user);
        try {
            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            System.out.println("receiver success");
        } catch (Exception e) {
            e.printStackTrace();
            //丢弃这条消息
            //channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
            System.out.println("receiver fail");
        }

    }

}
