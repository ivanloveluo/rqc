package com.rq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RqcApplication {

	public static void main(String[] args) {
		SpringApplication.run(RqcApplication.class, args);
	}

}
